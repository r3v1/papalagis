# Los Papalagi

## Resumen
Es una colección de discursos que el jefe samoano Tuiavii de Tiavea dirige a 
sus conciudadanos, en los que describe un supuesto viaje por Europa en el 
periodo justamente anterior a la Primera Guerra Mundial. En ellos el jefe 
samoano interpreta la cultura occidental (la de los papalagis, u hombres 
blancos, en lengua samoana) desde la perspectiva de un nativo, criticando la 
deshumanización y el materialismo de la sociedad europea, y describiendo con 
ingenuidad elementos tales como el dinero o el teléfono. 

Tuiavii de Tiavea previene a los samoanos para que no se dejen contaminar por 
el influjo de la cultura europea.

## Autor
Libro escrito por Erich Scheurmann y publicado en 1920. 

## Nota
No me atribuyo la autoría de la obra, sí de la edición orientada a este formato.